#!/usr/bin/ruby

class Setting

  def nick
    :etalan
  end

  def nick_alt
    :etalan_
  end

  def hostname
    :agilob
  end

  def servername
    :imabot
  end

end
