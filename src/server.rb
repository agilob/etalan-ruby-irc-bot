#!/usr/bin/ruby

require 'socket'
require 'openssl'
require 'sqlite3'
require 'rubygems'
require './logging.rb'

require_relative './Message.rb'
require_relative './channel'
load './config.rb'

# database to keep authentication data here
@@access = SQLite3::Database.open('methods/access.sqlite3')
# here i keep data about messages to deliver in the future
@@leave = SQLite3::Database.open('methods/tell.sqlite3')

# A object with methods to connect to a server (address and port are sent by constructor).
# Listens in socket, sends messages to proper channel, filters some data for more specific methods.
class Server

  #A list of channels where bot is in.
  @@channels = []

  # A hash list of nicks user => channel, to deliver public messages.
  @@hashListOfNicks = Hash.new

  # Creates itself, chooses to use a secure connection or not.
  # TODO This def must be rewritten to improve code quality and make it
  # shorter
  def initialize(address, port, ssl)

    @set=Setting.new

    @address = address
    @port = port
    @ssl = false if ssl != 1
    @ssl = true if ssl == 1

    if @ssl == true
      sslConnect
    elsif @ssl == false
      connect
    end #if

    @@times=0
    loop do

      raw = @@socket.gets
      puts raw

      if raw.include?('PING')
        pong = raw.gsub('PING :', '').gsub(/\s/, '')

        replay "PONG :#{pong}"

        sleep(4) #waits for AUTH

        if @@times == 1 or @@times == 0
          join('frytki') #if @@times == 1
          @@times = @@times+1
        end

        ## end of must be removed!!

      elsif raw.include?(' PRIVMSG ')

        nick = Message.getNick raw

        channel = Message.getSource raw

        message = Message.getMessage raw

        puts channel+' '+'<'+nick+'>'+' '+message

        Thread.new do
          sendToOperate(channel, nick, message)
        end

      elsif raw.include?(" 353 #{@set.nick} @ #")

        channel = raw[raw.index("#")..raw.index(" :")-1]
        filterListOfNicks raw, channel

      elsif raw.include?(' JOIN ') and not raw.include?('You are not registered')

        nick = raw[1 .. raw.index('!')-1]
        channel = raw.scan(/["#"]\w*/)

        #names #for control purposes
        #channel[0] - WHY CHANNEL IS AN ARRAY??
        @@hashListOfNicks.update(nick => channel[0])
        checkIfMsg nick, @@hashListOfNicks[nick]

      elsif raw.include?(' PART #')
        @@hashListOfNicks.clear
        names
      end #if include(PING/PRIVMSG/NAMES)
    end #loop
  end

  #initialize


  # Redirects a message to a proper operator.
  def sendToOperate channel, author, message

    if channel.match(/^\#/)
      @@channels.each { |channelFromArray|
        if channelFromArray.getName == channel
          channelFromArray.getMessage(author, message)
        end
      }
    else

    end
  end

  #send to channel


  # Connects to a server on TCP/IP without SSL.
  def connect
    @@socket = TCPSocket.new(@address, @port)

    replay("NICK #{@set.nick_alt}")
    replay("USER #{@set.nick_alt} #{@set.hostname} #{@set.servername} : #{@set.nick_alt} ")
  end

  #connect


  # Connects to a server on SSL(TCP/IP).
  def sslConnect

    @@socket = TCPSocket.new(@address, @port)

    ssl_context = OpenSSL::SSL::SSLContext.new()

    unless ssl_context.verify_mode
      warn 'Verifying is off.'
      ssl_context.verify_mode = OpenSSL::SSL::VERIFY_NONE
    end

    @send = OpenSSL::SSL::SSLSocket.new(@@socket, ssl_context)
    @send.sync_close = true
    @send.connect

    replay("NICK #{@set.nick}")
    replay("USER #{@set.nick} #{@set.nick2} #{@set.nick}2 #{@set.nick}3")

  end

  #sslConnect


  # Gets list of nicks from all channels where bot has joined.
  def getListOfNicks
    @@hashListOfNicks
  end

  #get list of nicks


  # Joins the channel, creates Channel object.
  def join(name)
    Logging.createDB name
    @@channels << Channel.new(name)
  end

  #join


  # Sends raw message to a server.
  def replay(message)
    @@socket.puts message
  end

  #replay


  # Sends a message to a channel/user. More detailed than replay()
  def privmsg(channel, message)
    @@socket.puts "PRIVMSG #{channel} : #{message}" if @@socket != nil
    sleep(1)
  end

  #privmsg


  # Sends a requests to get a list of nicks on each channel.
  def names
    @@channels.each { |kanal|
      replay "NAMES #{kanal.getName}"
    }
  end


  # Filters a raw message to an array of users online.
  def filterListOfNicks(raw, channel)

    raw = raw[(raw.index(channel) + ":#{channel}".length+1) .. -1]
    @nicks = raw.split(' ') #converting into array of nicks
    @nicks.each { |nick|
      if nick.match(/\W/)
        nick = nick[1..-1] #remove special char from nick
      end
      @@hashListOfNicks.update({nick => channel})
    }
  end


  # Method connects to a database, saves messages for offline users, or to users on an other channel.
  def tell(parameters)

    names

    sender = parameters[0]
    receiver = parameters[1]
    message = parameters[2..-1]

    if message.nil? or message.length >= 511
      warn 'Message too long or too short!'

    else
      message = message.join(' ').to_s.gsub('"', '\0\0') # replace
      @@leave.execute("INSERT OR REPLACE INTO leave_message (sender, receiver, message)
                          VALUES (\"#{sender}\", \"#{receiver}\", \"#{message}\");")
    end

    checkIfOnline receiver

  end


  # Checks if requested nick is on any of channels.
  def checkIfOnline(nick)
    if @@hashListOfNicks.include?(nick)
      checkIfMsg nick, @@hashListOfNicks[nick] #channel of provided nick
    end
  end


  # Checks if requested user has any messages.
  def checkIfMsg(nick, channel)

    result = @@leave.execute("SELECT id, sender, message FROM
                              leave_message WHERE received == 'false' AND receiver == '#{nick}';")
    result.each { |row|

      privmsg("#{channel}", "#{nick} od #{row[1]} : #{row[2]}")
      @@leave.execute("UPDATE leave_message SET received='true' WHERE id=#{row[0]};")

    }

  end


  private :sendToOperate, :connect, :sslConnect
  private :checkIfOnline, :checkIfMsg, :getListOfNicks, :names
  protected :join, :tell, :replay, :privmsg

end #class Server