#!/usr/bin/ruby
require 'sqlite3'
require 'rubygems'

# class where logs from chats are written.
# saves them into a database to 
class Logging

  @messages = SQLite3::Database.open('methods/message.sqlite3')

  def self.createDB channel
    @messages.execute("CREATE TABLE IF NOT EXISTS
                        '#{channel}'(id integer primary key autoincrement,
                        author varchar,
                        message varchar,
                        datetime DATE DEFAULT current_timestamp,
                        hour INT,
                        number_of_chars INT);")
  end


  def self.insertLog channel, nick, msg

    channel = channel[1..-1]
    msg = msg.chop

    if nick.include?('"') or msg.include?('"')
      nick = nick.gsub('"', '""')
      msg = msg.gsub('"', '""')
    end

    @messages.execute("INSERT INTO #{channel}
                       (author, message, datetime, hour, number_of_chars) VALUES
                       (\"#{nick}\", \"#{msg}\", current_timestamp,
                       (strftime('%H', 'NOW', 'localtime')), #{msg.length});"
    )
  end

end
