#!/usr/bin/ruby

# Class is about to filter data from *raw* message.
# It could be in server.rb but it's nicer here.
class Message

  public

  # Filters nick from message
  def self.getNick raw
    if (raw.include?('!'))
      nick = raw[1..raw.index('!')-1]
    else
      nick = :info
    end
    return nick
  end


  # Filters message (content) from raw message
  def self.getMessage raw
    message = raw[raw.index(' :')+2 .. -1]
    return message
  end


  # Filters a source of message channel/priv
  def self.getSource raw
    if raw.match('PRIVMSG #')
      source = raw[raw.index("#")..raw.index(" :")-1]
    elsif raw.match('PRIVMSG mubier')
      source = getNick raw
    end
    return source
  end

end