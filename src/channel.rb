#!/usr/bin/ruby

require 'uri'
require 'sqlite3'
require 'net/http'
require 'rubygems'

require_relative './server.rb'
require './logging.rb'

# A class inherits some methods from Server.
# An objects of this class is an channel where bot is connected.
# TODO this class must be rewritten
class Channel < Server

  # Joins a channel
  def initialize channelName
    @channelName = '#' + channelName # making #channel format
    replay 'JOIN ' + @channelName
    loadAvailableMethods
  end


  # Makes a list of methods which bot can execute
  # TODO load a list of files from directory
  # each of the files will be new method
  # aka plug-ins system
  def loadAvailableMethods
    @@methods=Channel.public_instance_methods(false).each { |x|
      x = x.to_s
    }
  end


  # Finishes executing of bot if command was send by admin
  def quit(parameters)
    if isAdmin parameters[0].to_s
      exit
    end
  end


  # How can I help you?
  # this method will probably send a list of all
  # methods available by this bot
  # TODO later... list of methods
  def help(parameter)
    privmsg @channelName, 'how can i help you?'
  end


  # After getting a message manages its.
  def getMessage(author, message)
    @author = author.to_s
    @message = message.to_s

    #Logging.insertLog @channelName, @author, @message
    executeIfPrivileged @author, @message

  end

  # Checks if the user is admin.
  def isAdmin(commander)

    result=@@access.execute("SELECT active from admin_bot
                              WHERE nick=='#{commander}';")
    puts (result.join.to_s == '1' or result.join.to_s == :true)
    return (result.join.to_s == '1' or result.join.to_s == :true)

  end


  # Adds or removes access to bot.
  def a_r_access(commander, parameters)

    if parameters[0] == 'add'
      @@access.execute("INSERT OR REPLACE INTO access_bot
                          (nick, active, given_by, last_change_date)
                          VALUES (\"#{parameters[1]}\", 1,
                          \"#{commander}\", current_timestamp)")

      warn "Privileges added for #{parameters[1]}."
      privmsg(@channelName, "#{parameters[1]} zyskal uprawnienia.")

    elsif parameters[0] == 'remove'
      @@access.execute("INSERT OR REPLACE INTO access_bot
                          (nick, active, given_by, last_change_date)
                          VALUES (\"#{parameters[1]}\", 0, \"#{commander}\", current_timestamp)")

      warn "Privileges removed from #{parameters[1]}."
      privmsg(@channelName, "#{parameters[1]} stracil uprawnienia.")
    end

  end


  # Checks if the user has access to the bot.
  def hasAccess(nick)
    result = @@access.execute('SELECT nick FROM access_bot WHERE active=1;')
    return result.to_a.join(' ').to_s.include?(nick)
  end


  # Checks if user is privileged and executes command
  def executeIfPrivileged(user, message)

    if ((hasAccess(user)) == true or (isAdmin(user)) and message.match(/^\!/))

      command = message.split(' ')
      command[0] = command[0][1..-1] #removes ! from first string in the array

      if command[0] != 'access'
        execute(command)
      elsif command[0] == 'access' and isAdmin user
        a_r_access(user, command[1..-1])
      end #command == access
    end #if has access or is admin

    execute command

  end


  # Executes provide command with data
  def execute(parameters)

    command = parameters

    @@methods.each { |s|
      if s.to_s == command[0]
        command.insert(1, @author)
        send(command[0], command[1..-1])
      end
    }
  end


  # Sends a name of Channel object.
  def getName()
    @channelName
  end


  def paste(message)

    message = message.to_s

    url = 'http://agilob.net/irc/script.php'
    uri = URI('http://agilob.net/irc/script.php')

    http = Net::HTTP.new(uri.host, uri.port)
    html = Net::HTTP.post_form(uri, {"HTMLString" => "#{message}"})

    puts output

    output = html.body.to_s
    output = 'http://agilob.net/irc/' + output

    privmsg(@author, output)

  end


  def sqlite(query)
    @db = SQLite3::Database.new('IDDB/iddb.sqlite3')

    query.shift
    query = query.join(' ')

    puts query

    string = '<table><th style="text-align: center;" colspan="7">' + query + '</th>'
    string = '<th>ALG</th><th>e</th><th>n</th><th>h</th><th>s</th><th>p</th><th>so</th>'

    results = @db.execute(query)

    results.each { |row|
      data = "<tr><td>#{row[0]}</td><td>#{row[1]}</td><td>#{row[2]}</td>
              <td>#{row[3]}</td><td>#{row[4]}</td><td>#{row[5]}</td>
              <td>#{row[6]}</td><td>#{row[7]}</td></tr>\n"

      string = string + data
    }

    string = string + '</table>'

    @db.close

    paste(string)

  end


  def iddb(query)
    @db = SQLite3::Database.new('IDDB/iddb.sqlite3')

    query.shift
    query = query.join(' ')

    puts query

    query = "select * from data where #{query};"

    puts query

    string = '<table><th style="text-align: center;" colspan="7">' + query + '</th>'
    string = '<th>ALG</th><th>e</th><th>n</th><th>h</th><th>s</th><th>p</th><th>so</th>'

    results = @db.execute(query)

    results.each { |row|
      data = "<tr><td>#{row[0]}</td><td>#{row[1]}</td><td>#{row[2]}</td>
              <td>#{row[3]}</td><td>#{row[4]}</td><td>#{row[5]}</td>
              <td>#{row[6]}</td><td>#{row[7]}</td></tr>\n"

      string = string + data
    }

    string = string + '</table>'

    @db.close

    paste(string)

  end


  # Gives an operator status to a message author.
  def up parameters
    replay "MODE #{@channelName} +o #{parameters[0]}"
  end


  # Takes an operator status from a person.
  def down parameters
    replay "MODE #{@channelName} -o #{parameters[1]}"
  end


  # Invites nick to the channel
  def invite parameters
    replay "INVITE #{@channelName} #{parameters[0]}"
  end


  # Kicks person from channel
  def kick parameters
    if isAdmin @author == true
      replay "KICK #{@channelName} #{parameters[0]}"
    end
  end


  private :help, :isAdmin, :a_r_access, :hasAccess, :loadAvailableMethods
  public :quit, :help, :tell, :getName, :getMessage

end